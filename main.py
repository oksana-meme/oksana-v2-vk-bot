import os

from dotenv import load_dotenv

import requests
import vk_api
from vk_api.bot_longpoll import VkBotEventType, VkBotLongPoll

load_dotenv()

vk_token = os.getenv('VK_BOT_TOKEN')


def start():
    print('start VKBot')
    vk_session = vk_api.VkApi(token=vk_token)

    longpoll = VkBotLongPoll(vk_session, 209759364)
    vk_session.get_api()

    for event in longpoll.listen():
        if event.type == VkBotEventType.MESSAGE_NEW:
            print("=======================")
            print(event.object.message)
            attempt = 1
            status = False

            while attempt <= 3 and not status:
                try:
                    print(event.type)
                    author = vk_session.method("users.get", {
                        "user_ids": event.object.message['from_id']
                    })[0]

                    try:
                        message = vk_session.method("messages.getByConversationMessageId", {
                            "peer_id": event.object.message['peer_id'],
                            "conversation_message_ids": event.object.message['conversation_message_id'],
                            "extended": 1
                        })['items'][0]
                    except:
                        message = event.object.message

                    if not message or not message['attachments']:
                        status = True
                        continue

                    response = requests.post(os.getenv('TELEGRAM_SENDER_DOMAIN') + 'vk/message', json={
                        'message': message,
                        'author': author
                    })
                    print(response)

                    status = True
                except Exception as e:
                    print('--- longpoll ---')
                    print(e)
                    print(str(e))
                    print('--- longpoll ---')
                finally:
                    attempt += 1

            print("=======================")


if __name__ == '__main__':
    while True:
        try:
            start()
        except Exception as exception:
            print('main error: ' + str(exception))
