# Docker run
```shell
docker-compose up --force-recreate --build
```

# Docker network

```shell
docker network create -d bridge oksana-v2
```